package com.example.demo.service;

import com.example.demo.model.Producto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductoService {

    public List<Producto> getProductos(){
        List<Producto>productos = new ArrayList<>();
        productos.add(1,new Producto("tallarines", "comestibles",
                20,2500,"aa"));
        productos.add(1,new Producto("porotos", "comestibles",
                50,2500,"aa"));

        return productos;
    }
}
