package com.example.demo.controller;

import com.example.demo.model.Merma;
import com.example.demo.model.Reponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("mermas")
public class MermaController {

    private List<Merma> mermas;
    public MermaController(){
        this.mermas = new ArrayList<>();
        mermas.add(new Merma("Perdida de carne","caducación de los productos",15));
        mermas.add(new Merma("Perdida de pan","caducación de los productos",22));
        mermas.add(new Merma("Perdida de helados","robo",55));
    }

    @GetMapping("")
    public List<Merma> getMermas(){
        return mermas;
    }

    @RequestMapping("{id}")
    public Merma getMerma(@PathVariable int id){
        return mermas.get(id);
    }

}
