package com.example.demo.controller;

import com.example.demo.model.Cajero;
import com.example.demo.model.Reponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("cajeros")
public class CajeroController {

    private List<Cajero>cajeros;
    public CajeroController(){
        cajeros = new ArrayList<>();
        cajeros.add(new Cajero("Jorge Cortez", "20213123-2", "cajero"  ,"1234", 21));
        cajeros.add(new Cajero("Agustin Manriquez", "13213123-2", "cajero"  ,"3211", 29));
        cajeros.add(new Cajero("Rodrigo Brevis", "11213123-2", "cajero"  ,"123132", 54));
        cajeros.add(new Cajero("Alejandro Mella", "15213123-2", "cajero"  ,"3312312", 67));
        cajeros.add(new Cajero("Ricardo Johnson", "20213123-2", "cajero"  ,"54121", 12));
    }

    @GetMapping("")
    public List<Cajero>getCajeros(){
        return cajeros;
    }

    @RequestMapping("{id}")
    public Cajero getCajero(@PathVariable int id){
        return cajeros.get(id);
    }
}
