package com.example.demo.controller;

import com.example.demo.model.Producto;
import com.example.demo.model.Reponedor;
import com.example.demo.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("productos")
public class ProductoController {

    private List<Producto> productos;
    public ProductoController(){
        this.productos = new ArrayList<>();
        productos.add(new Producto("Vino Santa Helena","bebestibles",50,2500,"pasillo 2"));
        productos.add(new Producto("Pisco Mistral","bebestibles",13,6000,"pasillo 2"));
        productos.add(new Producto("Galletas Serranita","dulces",21,560,"pasillo 1"));
        productos.add(new Producto("Galletas Nick","dulces",15,560,"pasillo 1"));
        productos.add(new Producto("Yogurt Nestle","refrigerados",60,250,"pasillo 8"));
        productos.add(new Producto("Ketchup JB","salsas",36,2500,"pasillo 4"));
        productos.add(new Producto("Mostaza JB","salsas",40,2500,"pasillo 4"));
        productos.add(new Producto("Mayonesa JB","salsas",29,2500,"pasillo 4"));
    }

    @GetMapping("")
    public List<Producto> getProductos(){
        return productos;
    }

    @RequestMapping("{id}")
    public Producto getProducto(@PathVariable int id){
        return productos.get(id);
    }

}
