package com.example.demo.controller;

import com.example.demo.model.Cajero;
import com.example.demo.model.Reponedor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("reponedores")
public class ReponedorController {

    private List<Reponedor> reponedores;
    public ReponedorController(){
        reponedores = new ArrayList<>();
        reponedores.add(new Reponedor("Alberto Cortez", "20213123-2","cajero"  , "1234", "dulces"));
        reponedores.add(new Reponedor("Maria Lopez", "13213123-2","cajero"  , "3211", "hogar"));
        reponedores.add(new Reponedor("Sandra Cid", "11213123-2", "cajero"  ,"123132", "bebestibles"));
    }

    @RequestMapping("")
    public List<Reponedor> getReponedores(){
        return reponedores;
    }

    @RequestMapping("{id}")
    public Reponedor getReponedor(@PathVariable int id){
        return reponedores.get(id);
    }
}
