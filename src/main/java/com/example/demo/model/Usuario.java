package com.example.demo.model;

public class Usuario {
    protected String nombre;
    protected String rut;
    protected String contraseña;

    public Usuario(String nombre, String rut, String contraseña) {
        this.nombre = nombre;
        this.rut = rut;
        this.contraseña = contraseña;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
}
