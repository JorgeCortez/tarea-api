package com.example.demo.model;

public class Reponedor extends Usuario{
    private String seccion;
    private String usuario;

    public Reponedor(String nombre, String rut, String usuario, String contraseña, String seccion) {
        super(nombre, rut, contraseña);
        this.seccion = seccion;
        this.usuario = usuario;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}

//Nombre, rut, usuario, contraseña, sección
