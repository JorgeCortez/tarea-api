package com.example.demo.model;

public class Cajero extends Usuario{
    private int ventas;
    private String usuario;

    public Cajero(String nombre, String rut, String usuario, String contraseña, int ventas) {
        super(nombre, rut, contraseña);
        this.ventas = ventas;
        this.usuario = usuario;
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}

//Nombre, rut, usuario, contraseña, ventas.
